let cachefunction = (cb) => {
  let cache = {};
  return (...data) => {
    if (cache.hasOwnProperty(data) === false) {
      let storeData = cb(...data);
      cache[data] = storeData;
      return "invoked data = " + storeData;
    } else {
      let cachedData = cache[data];
      return "Cached data = " + cachedData;
    }
  };
};

module.exports = cachefunction;
