let counterFactory = function () {
  let count = 0;
  return {
    increment: () => {
      return ++count;
    },
    decrement: () => {
      return --count;
    },
  };
};

module.exports = counterFactory;
