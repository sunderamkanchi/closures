let limitFunctionCallCount = (cb, n) => {
  let count = 0;
  return (...data) => {
    if (count < n) {
      count += 1;
      return cb(...data);
    } else {
      return null;
    }
  };
};

module.exports = limitFunctionCallCount;
