let limitFunctionCallCount = require("../limitFunctionCallCount");

const doMath = (a, b, c) => a + b - c;

const limitedMathFn = limitFunctionCallCount(doMath, 3);

console.log(limitedMathFn(1, 2, 3)); // 0
console.log(limitedMathFn(3, 4, 5)); // 2
console.log(limitedMathFn(4, 5, 1)); // 8
console.log(limitedMathFn(4, 5, 6)); // null
