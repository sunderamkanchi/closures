let cacheFunction = require("../cacheFunction");
const doMath = (a, b, c) => a + b - c;

const cachedDoMath = cacheFunction(doMath);

console.log(cachedDoMath(1, 2, 3)); // doMath should be invoked
console.log(cachedDoMath(1, 2, 3)); // doMath should not be invoked but the same result returned from the cache
console.log(cachedDoMath(1, 3, 2)); // doMath should be invoked
console.log(cachedDoMath(2, 3, 4)); // doMath should be invoked
console.log(cachedDoMath(2, 3, 4)); // doMath should not be invoked but the same result returned from the cache
